CMAKE_MINIMUM_REQUIRED(VERSION 3.4)

PROJECT(Symbolics)


#IF(WIN32)
    # deaktiviere Optimierung fuer RelWithDebInfo
#	if(DEFINED CMAKE_C_FLAGS_RELWITHDEBINFO)
#		STRING(REPLACE "O2" "Od" CMAKE_C_FLAGS_RELWITHDEBINFO ${CMAKE_C_FLAGS_RELWITHDEBINFO})
#    endif(DEFINED CMAKE_C_FLAGS_RELWITHDEBINFO)
#	if(DEFINED CMAKE_CXX_FLAGS_RELWITHDEBINFO)
#		STRING(REPLACE "O2" "Od" CMAKE_CXX_FLAGS_RELWITHDEBINFO ${CMAKE_CXX_FLAGS_RELWITHDEBINFO})
#	endif(DEFINED CMAKE_CXX_FLAGS_RELWITHDEBINFO)
#ENDIF(WIN32)


# gcc ab 4.7 ist echt unkooperativ mit Forward-Deklarationen und Templates
IF(NOT WIN32)
  ADD_DEFINITIONS("-fpermissive")
ENDIF(NOT WIN32)

# RootDirectory
SET(ROOTDIR ${CMAKE_CURRENT_SOURCE_DIR})

# Python 
find_program(PYTHON_EXECUTABLE NAMES python)
MESSAGE("Python is " ${PYTHON_EXECUTABLE})

#################################################################
MACRO(PYTHONTEST name dir)
IF(NOT ${CMAKE_VERSION} VERSION_LESS 2.8.3)
   add_test (NAME PyTest_${name} 
             WORKING_DIRECTORY ${dir}
			 COMMAND ${PYTHON_EXECUTABLE} ${name}.py)
ELSE()
	MESSAGE( "Python Test ${name} skipped! CMake Version > 2.8.3 required." )
ENDIF()
ENDMACRO(PYTHONTEST)
#################################################################

# Tests
OPTION(RUN_TESTS  "Run Tests"  ON)
IF(RUN_TESTS)
ENABLE_TESTING()
ENDIF(RUN_TESTS)

INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/include)
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/Functions/include)
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/Printer/include)
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/Graph/include)
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/Writer/include)


SET(symbolics_headers     include/Basic.h
                        include/BinaryOp.h
                        include/Zero.h
						include/Eye.h
						include/Int.h
						include/Real.h
						include/Bool.h
                        include/Error.h
                        include/Factory.h
                        include/Matrix.h
                        include/NaryOp.h 
                        include/Shape.h 
                        include/str.h 
                        include/Symbol.h 
                        include/Symbolics.h
                        include/SymmetricMatrix.h
                        include/UnaryOp.h
                        include/Filesystem.h
                        include/intrusive_ptr.h)

SET(symbolics_sources     Basic.cpp 
                        BinaryOp.cpp
                        Zero.cpp
						Eye.cpp
						Int.cpp
						Real.cpp
						Bool.cpp
                        Factory.cpp
                        Matrix.cpp 
                        NaryOp.cpp 
                        Shape.cpp 
                        str.cpp 
                        Symbol.cpp 
                        SymmetricMatrix.cpp
                        UnaryOp.cpp
                        Filesystem.cpp)

IF(WIN32)
ELSE()
	ADD_DEFINITIONS(-fPIC)
ENDIF()

ADD_LIBRARY( Symbolics STATIC ${symbolics_sources} ${symbolics_headers} )
TARGET_LINK_LIBRARIES( Symbolics Functions )

ADD_SUBDIRECTORY( Functions )
ADD_SUBDIRECTORY( Printer )
ADD_SUBDIRECTORY( Test )
ADD_SUBDIRECTORY( SympyWrapper )
ADD_SUBDIRECTORY( Graph )
ADD_SUBDIRECTORY( Writer )

