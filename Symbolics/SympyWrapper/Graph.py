'''
This file is part of PyMbs.

PyMbs is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

PyMbs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with PyMbs.
If not, see <http://www.gnu.org/licenses/>.

Copyright 2011, 2012 Carsten Knoll, Christian Schubert,
                     Jens Frenkel, Sebastian Voigt
'''

from PyMbs.Symbolics import symbolics    # C++ Module
from PyMbs.Symbolics.symbolics import CSymbol as Variable
from PyMbs.Symbolics.symbolics import der, scalar

import numpy

PEDANTIC = False

class VarAlreadyExistsError(BaseException):
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return self.name


class VarUnknownError(BaseException):
    def __init__(self, name):
        self.name = name
    def __str__(self):
        return self.name


class EquationAlreadyExistsError(BaseException):
    def __init__(self, lhs, rhs, eqn):
        self.lhs = lhs
        self.rhs = rhs
        self.eqn = eqn
    def __str__(self):
        return "'" + str(self.lhs) + " == " + str(self.rhs) + "' conflicts with \r\n " + \
               "'" + str(self.eqn.lhs) + " == " + str(self.eqn.rhs) + "'!"


class ShapeError(BaseException):
    def __init__(self, lhs, lshape, rhs, rshape):
        self.lhs = lhs
        self.lshape = lshape
        self.rhs = rhs
        self.rshape = rshape
    def __str__(self):
        return "Shape mismatch '%s!=%s' at "%(str(self.lshape), str(self.rshape)) + \
                                              str(self.lhs) +" == " + str(self.rhs)

class VarKind:
    Variable = 0x01     # Normal Variables
    Constant = 0x02     # An Variable which never change his value
    Parameter = 0x04       # Parameters
    Input = 0x08       # Input
    State = 0x10        # Map State to Generalised Coordinates/Constraint Forces/Lagrangian Multipliers
    Der_State = 0x20    # State Derivative (for Internal Use only)
    User_Exp = 0x40     # Expressions Defined by the User
    Sensor = 0x100      # Sensor
    Graphics = 0x200    # Graphical Sensor
    Controller = 0x400  # Controller


class Assignment(object):
    '''
    saves an assignment where lhs, may consist of more than one variable
    '''

    def __init__(self, lhs, rhs, varKind, shape, graph):

        # Unterstuetzen von numpy Typen
        if isinstance(rhs, numpy.matrix):
            rhs = rhs.tolist()

        # Pruefen der rechten Seite
        assert isinstance(rhs, (symbolics.CBasic, list)), \
            "rhs must be a 'CBasic' not a '%s' with %s"%(str(rhs.__class__), str(rhs))

        if (PEDANTIC):
            # Atoms extrahieren

            if isinstance(rhs, symbolics.CBasic):
                rhs = [rhs]
            for r in rhs:
                atoms = r.atoms()

                # Alle Atoms durchlaufen
                for a in atoms:
                    if isinstance(a, symbolics.CSymbol):
                        assert str(a) in graph.variables, "Die Variable '%s' ist unbekannt"%(str(a))

        if isinstance(rhs, symbolics.CBasic):
            if (shape is None):
                rshape = graph.getShape(rhs)
            else:
                rshape = shape
        else:
            if (shape is None):
                # Shape berechnen
                if (len(rhs)==1):
                    rshape = tuple()
                    if (PEDANTIC):
                        rhs = rhs[0]
                else:
                    rshape = (len(rhs),)
            else:
                rshape = shape

        if lhs is not None:
            assert isinstance(lhs,(list,str,Variable,der)), "lhs (%s) must be either list, str or variable but is %s"%(str(lhs),str(lhs.__class__))

        # Haben wir eine Liste?
        if (isinstance(lhs, list)):
            # ggf. Variablen anlegen
            for i in range(len(lhs)):
                if (isinstance(lhs[i],str)):
                    lhs[i] = graph.addVariable(name=lhs[i], shape=tuple(), varKind=varKind)
                assert isinstance(lhs[i], (Variable,der))
            # Shape berechnen
            if (len(lhs)==1):
                lshape = tuple()
                lhs = lhs[0]
            else:
                lshape = (len(lhs),)
                self.lhs = lhs

        else:
            # Falls lhs ein string ist, eine Variable anlegen
            if (isinstance(lhs, str)):
                lhs = graph.addVariable(name=lhs, shape=rshape, varKind=varKind)
            if lhs is not None:
                assert isinstance(lhs, (Variable,der)), "Got lhs=%s (%s)"%(lhs, lhs.__class__)
            lshape = rshape

        if (lshape != rshape):
            if ((lshape == ()) and \
               ((rshape == (1,)) or (rshape == (1,1))) ) :
                rhs = scalar(rhs)
            else:
                raise ShapeError(lhs, lshape, rhs, rshape)

        self.lhs = lhs
        self.shape = rshape
        self.rhs = rhs



class Graph(object):
    '''
    Interface to the C++ Graph Object
    '''

    def __init__(self):
        '''
        Constructor
        '''
        if (PEDANTIC):
            self.variables = dict()
            print("Graph is in Pedantic Mode. May be painfully slow!!!")
        self.cgraph = symbolics.CGraph()

#        self.nGetShape = 0
#        self.nAddVariable = 0
#        self.nAddEquation = 0


    def addVariable(self, name, shape=None, varKind=VarKind.Variable, initialValue=None, comment=None, **kwargs):
        '''
        Creates a new variable
        '''

#        self.nAddVariable = self.nAddVariable + 1

        # shape is None => shape is scalar
        if (shape is None):
            shape = tuple()

        assert isinstance(name, str), "name must be a string"
        assert isinstance(shape, tuple), "shape must be tuple"
        assert isinstance(varKind, int), "varKind must be an integer"
        if (comment is None): comment = name
        assert isinstance(comment, str), "comment must be a string"

        if (PEDANTIC):
            # Look for Duplicates
            if (name in self.variables):
                raise VarAlreadyExistsError(name)

        # Hand it over to the graph
        var = self.cgraph.addVariable(name, shape, varKind, initialValue, comment)

        # Add it to the list
        if (PEDANTIC):
            self.variables[str(var)] = var

        # return it
        return var


    def getVariable(self, name):
        '''
        Return variable
        '''
        return self.cgraph.getVariable(name)


    def getAssignments(self, kind):
        '''
        Return all Assignments for a specific kind (DER_STATE or SENSOR)
        '''
        assert kind in (VarKind.Der_State, VarKind.Sensor), 'kind must be DER_STATE or SENSOR'
        return self.cgraph.getAssignments(kind)


    def getVariables(self, kind):
        '''
        Return variables with kind
        '''
        return self.cgraph.getVariables(kind)


    def getinitVal(self, symbol):
        '''
        Return variable init value
        '''
        return self.cgraph.getinitVal(symbol)


    def addEquation(self, lhs, rhs, varKind=VarKind.Variable, shape=None, implicit=False):
        '''
        Add a new Equation
        '''

#        self.nAddEquation = self.nAddEquation + 1

        assert isinstance(varKind, int), "varKind must be an integer, not a %s"%(str(varKind.__class__))

        # Cast
        if (isinstance(rhs, (int,float))):
            rhs = symbolics.CNumber(rhs)

        # neues Assignment
        ass = Assignment(lhs, rhs, varKind, shape, self)

        # Assignment an den Graphen weitergeben
        try:
            self.cgraph.addEquation(ass.lhs, ass.rhs, implicit)
        except symbolics.Error as ex:
            raise symbolics.Error("%s\nError while adding equation %s = %s, (%s = %s)"%(str(ex), ass.lhs, ass.rhs, ass.lhs.__class__, ass.rhs.__class__))

        return ass.lhs


    def getShape(self, exp):
        '''
        Return the shape of an expression
        '''

        # Abkuerzen fuer einfache Typen
        if (isinstance(exp, (int,float))):
            return tuple()
        if (isinstance(exp, symbolics.CBasic)):
            return exp.shape()
        else:
            raise TypeError("Class %s has no shape!"%str(exp.__class__))

#        self.nGetShape = self.nGetShape + 1
#        return self.cgraph.getShape(exp)


    def buildGraph(self, optimize=True):
        '''
        Disable Optimization in Graph
        '''
        assert isinstance(optimize, bool), "optimize must be a bool!"
        return self.cgraph.buildGraph(optimize)


    def writeCode(self, typeStr, name, path, **kwargs):
        '''
        Write Graph to File
        '''

        assert isinstance(typeStr,str), "Type must be a string"
        assert isinstance(name,str), "name must be a string"
        assert isinstance(path,str), "path must be a string"

        return self.cgraph.writeOutput(typeStr, name, path, **kwargs)


#    def printStats(self):
#        print "addVariable: %s\naddEquation: %s\ngetShape: %s"%(self.nAddVariable,self.nAddEquation,self.nGetShape)
