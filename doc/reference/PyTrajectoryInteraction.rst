.. _interaction:

*****************************
Interaction with PyTrajectory
*****************************

`PyTrajectory <https://pytrajectory.readthedocs.org>`_ is a Python 
library for the determination of the feed forward control to achieve a 
transition between desired states of a nonlinear control system.

It is being developed at Dresden University of Technology at the 
Institute for Control Theory.

The aim of the following method is to provide a connection between the 
capabilities of PyMbs for creating and visualising a multibody system 
and those of PyTrajectory for determining a feed forward control. 

.. automodule:: PyMbs.utils.sympyexport
   :members:

.. literalinclude:: ../../Examples/PyTrajectoryExample/PyTrajectoryInteraction.py
