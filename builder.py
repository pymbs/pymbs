import os
import platform
import sys
from distutils import sysconfig

class Builder():
  def __init__(self, modname, srcdir, installdir, builder = None):
    # aktuelles Verzeichnis
    _c = os.getcwd()
    self.dir0 = _c

    # Modulname
    if sys.platform=='win32':
      self.modname = '%s.pyd' % modname
    else:
      self.modname = '%s.so' % modname

    # Build, Source und Installdir
    self.builddir = os.path.join(_c,'build',srcdir)
    self.srcdir = os.path.join(_c, srcdir)
    self.installdir = os.path.join(_c, installdir)
    self.installdir0 = installdir

    # Build-Verzeichnis anlegen (kann schon da sein)
    try:
      os.makedirs(self.builddir)
    except:
      pass

    # Builder
    self.builder = builder
    if self.builder is None:
      if sys.platform=='win32':
        self.builder = 'NMake Makefiles'
      else:
        self.builder = 'Unix Makefiles'

  def cmake(self, force = False, buildType = 'Release'):
    # Python-Lib bestimmen
    libdir = None
    if "conda" in sys.version:
        pyversion = "python{}.{}".format(sys.version_info[0], sys.version_info[1])

        # look which entry of sys.path endswith something like lib/python3.6
        mainpath = [p for p in sys.path if p.endswith(os.path.join("lib", pyversion))][0]
        prefix, _  = os.path.split(mainpath)
        #
        py_lib = os.path.join(prefix, "lib{}m.so".format(pyversion))

    elif sys.platform=='win32':
      py_lib = '%s/Libs/python%s%s.lib' % (sys.prefix, sys.version_info[0], sys.version_info[1])
    else:
      possible_paths = ['/usr/lib', # Debian Stable, Fedora, Suse
                        '/usr/lib64', # Fedora 64-bit
                        '/usr/lib/i386-linux-gnu', # 32-Bit Ubuntu, Debian Testing
                        '/usr/lib/x86_64-linux-gnu'] # 64-Bit Ubuntu, Debian Testing
      for path in possible_paths:
          fname = 'libpython%s.%s.so.1.0' % (sys.version_info[0], sys.version_info[1])
          if os.path.isfile(os.path.join(path, fname)):
              libdir = path
      if libdir is None:
          msg = "Could not find {}. This is probably easy to solve, " \
                "but you have to investigate yourself.".format(fname)
          raise FileNotFoundError(msg)
      py_lib = '%s/libpython%s.%s.so.1.0' % (libdir, sys.version_info[0], sys.version_info[1])
    py_lib = py_lib.replace("\\","/")

    # so far this has only be tested on debian 8 (stable)
    py_inc = sysconfig.get_python_inc().replace("\\","/")

    # TODO: Test ob CMakeCache da und mit force vergleichen
    os.chdir(self.builddir)
    cmake_args = (self.builder, py_inc, py_lib, buildType, self.installdir, self.srcdir)
    cmd = 'cmake -G \"%s\" -D PYTHON_INCLUDE_DIR=%s -D PYTHON_LIBRARY:FILEPATH=%s -D CMAKE_BUILD_TYPE=%s -D CMAKE_INSTALL_PREFIX=%s %s' % cmake_args
    os.system(cmd)

    # Verzeichnis wieder zurueckwechseln
    os.chdir(self.dir0)

  def make(self):
    os.chdir(self.builddir)
    if sys.platform=='win32':
      os.system('nmake')
    else:
      os.system('make')

    # Verzeichnis wieder zurueckwechseln
    os.chdir(self.dir0)

  def install(self):
    os.chdir(self.builddir)
    if sys.platform=='win32':
      os.system('nmake install')
    else:
      os.system('make install')

    # Verzeichnis wieder zurueckwechseln
    os.chdir(self.dir0)

  def all(self, buildType = 'Release'):
    self.cmake(True, buildType)
    self.install()

    # Modul zurueckgeben (mit /)
    s = os.path.join(self.installdir0, self.modname)
    s = s.replace("\\","/")
    return s


if __name__ == '__main__':
  b = Builder('symbolics', 'Symbolics', 'Symbolics/Sympywrapper')
  print('YYY: %s' % b.all())
